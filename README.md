# GitLab Support Toolkit

Support toolkit to help manage GitLab inventory 

# Quick Start

- Install software listed in the [Requirements](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-toolkit#requirements) section
- Clone this repo to your local workstation and switch to the cloned folder
- In this folder create a file `gitlab-vagrant.yml` with the required environments listed, see [Example](https://gitlab.com/gl-support/omnibus-vagrant/tree/master#example-gitlab-vagrantyml-inventory) for details
- Run `vagrant up name` where `name` corresponds to one instance names from `gitlab-vagrant.yml`

## Requirements:
[Vagrant 2.2.5+](https://www.vagrantup.com/downloads.html)

##### At least one of the following providers are required:
[Virtualbox 5.1+](https://www.virtualbox.org/wiki/Downloads)

[VMware Fusion](https://www.vmware.com/products/fusion/fusion-evaluation.html)

[An AWS Sandbox account](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/)

# Managing your virtual machines

_Note: Playbooks can be executed directly against non-virtual instances, see [playbooks](playbooks/README.md) for further information._

All instances are managed from a single inventory file _gitlab-vagrant.yml_ which resides in the root of the repository.

#### Usage

`vagrant {up|provision|ssh|halt|status|reload|destroy} <name_of_instance>`

## Omnibus Parameters

Deploy specific versions of Omnibus GitLab into a virtual environment.

Add parameters to file _gitlab-vagrant.yml_ in root of repository.

| Parameters   | Required             | Format       | Allowed Values       | Description |
| :----------- | :------------------- | :----------- | :------------------- | :---------- |
| name         | Yes                  | alphanumeric | Any                  |             |
| memory       | No (default 4096)    | numeric      | nnnn                 |             |
| version      | Yes                  | x.x.x        | n.n+.n               |             |
| edition      | Yes                  | alphanumeric | ce or ee             |             |

#### Example gitlab-vagrant.yml inventory

```yaml
---
- name: gitlab-ee-9-3-9
  version: 9.3.9
  edition: ee

- name: gitlab-with-es
  memory: 4272
  version: 9.3.9
  edition: ee
  elasticsearch: 5.3.1
```

Using the above example you can now create a new instance with `vagrant up gitlab-ee-9-3-9` or `vagrant up gitlab-with-es`.

#### Provision from .deb package (optional)

The installer will look for the .deb in `omnibus_packages/` before attempting any downloads from the repository, so if necessary drop your `.deb` in here to speed up the provisioning process.

#### Using the AWS provider

Go to your AWS console > ec2 > security groups and modify your default security group to allow HTTP, HTTPS, SSH. 
Set "source" to "My IP" for additional security.  You will only have to do this once unless your IP changes.

Install the `vagrant-aws` plugin with `vagrant plugin install vagrant-aws`.

Install a faux vagrant box with `vagrant box add dummy https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box`.

Add parameters to file _aws.yml_ in root of repository.


| Parameters              | Required             | Description | 
| :-----------            | :------------------- | :---------- |
| access_key              | Yes                  | [IAM credentials](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html) |
| secret_key              | Yes                  | [IAM credentials](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html) |
| region                  | Yes                  | [ec2 regions](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions) |
| keypair_name            | Yes                  | [ec2 key pairs ](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html)            |
| ami                     | Yes                  | [Ubuntu EC2 AMI locator](https://cloud-images.ubuntu.com/locator/ec2/)                              |
| instance_type           | Yes                  | [ec2 Instance Types](https://aws.amazon.com/ec2/instance-types/)                                    |
| ssh_path_to_private_key | Yes                  | Local path to the private key provided by keypair                                               |

Then launch with `vagrant {up|provision|ssh|halt|status|reload|destroy} <name_of_instance> --provider=aws` 

#### Connect to running instance

Use the _name_ parameter to SSH onto the machine:

```bash
vagrant ssh gitlab-example
```

## Additional services

Docker services can be deployed a standalone instance or alongside your Omnibus instances.

Deployed as docker containers onto the target instance.  This allows for ease of testing of GitLabs integrated apps.

_Note: Adding additional services is trivial, please visit [playbooks](playbooks/README.md#building-your-own-module-customization) for further information._

| Parameters   | Required             | Format       | Allowed Values       | URL |
| :----------- | :------------------- | :----------- | :------------------- | :---|
| name         | When standalone      | alphanumeric | Any                  |     |
| memory       | No (default 4096)    | numeric      | nnnn                 |     |
| squid_proxy  | No                   | alphanumeric | docker tag           | https://hub.docker.com/r/sameersbn/squid/tags/ |
| gitlab-runner| No                   | alphanumeric | docker tag           | https://hub.docker.com/r/gitlab/gitlab-runner/tags/ |
| elasticsearch| No                   | alphanumeric | docker tag           | https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html |

Example _gitlab-vagrant.yml_:

```yaml
- name: gitlab-runner
  memory: 1024
  gitlab_runner: v9.4.1
```
